package ru.vmaksimenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.enumerated.EventType;

import javax.jms.Connection;

public interface IActiveMQConnectionService {

    @NotNull Connection getConnection();

    void send(@NotNull Object entity, @NotNull EventType event);

}

package ru.vmaksimenkov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.AbstractEntityRecord;

import java.util.List;

public interface IAbstractRecordService<E extends AbstractEntityRecord> {

    @Nullable E add(@Nullable E entity);

    void add(@Nullable List<E> entities);

    void clear();

    boolean existsById(@Nullable String id);

    @NotNull
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    void remove(@Nullable E entity);

    void removeById(@Nullable String id);

    @NotNull Long size();

    void update(@NotNull E entity);

}

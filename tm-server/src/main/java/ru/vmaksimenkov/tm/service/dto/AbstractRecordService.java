package ru.vmaksimenkov.tm.service.dto;

import ru.vmaksimenkov.tm.api.service.dto.IAbstractRecordService;
import ru.vmaksimenkov.tm.dto.AbstractEntityRecord;
import ru.vmaksimenkov.tm.repository.dto.AbstractRecordRepository;

public abstract class AbstractRecordService<E extends AbstractEntityRecord, R extends AbstractRecordRepository<E>> implements IAbstractRecordService<E> {


}
package ru.vmaksimenkov.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.service.IPropertyService;

import static ru.vmaksimenkov.tm.constant.PropertyConst.*;

@Getter
@Service
@PropertySource("classpath:" + FILE_NAME)
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("${" + APPLICATION_VERSION_KEY + ":" + APPLICATION_VERSION_DEFAULT + "}")
    private String applicationVersion;

    @Value("${" + BACKUP_INTERVAL_KEY + ":" + BACKUP_INTERVAL_DEFAULT + "}")
    private int backupInterval;

    @NotNull
    @Value("${" + HIBERNATE_CACHEPROVIDER_CONFIG_KEY + ":" + HIBERNATE_CACHEPROVIDER_CONFIG_DEFAULT + "}")
    private String cacheProvider;

    @NotNull
    @Value("${" + HIBERNATE_FACTORY_CLASS_KEY + ":" + HIBERNATE_FACTORY_CLASS_DEFAULT + "}")
    private String factoryClass;

    @NotNull
    @Value("${" + HIBERNATE_FORMATSQL_KEY + ":" + HIBERNATE_FORMATSQL_DEFAULT + "}")
    private String formatSQL;

    @NotNull
    @Value("${" + JDBC_DIALECT_KEY + ":" + JDBC_DIALECT_DEFAULT + "}")
    private String jdbcDialect;

    @NotNull
    @Value("${" + JDBC_DRIVER_KEY + ":" + JDBC_DRIVER_DEFAULT + "}")
    private String jdbcDriver;

    @NotNull
    @Value("${" + JDBC_HBM2DDL_KEY + ":" + JDBC_HBM2DDL_DEFAULT + "}")
    private String jdbcHBM2DDL;

    @NotNull
    @Value("${" + JDBC_PASSWORD_KEY + ":" + JDBC_PASSWORD_DEFAULT + "}")
    private String jdbcPassword;

    @NotNull
    @Value("${" + JDBC_SHOW_SQL_KEY + ":" + JDBC_SHOW_SQL_DEFAULT + "}")
    private String jdbcShowSql;

    @NotNull
    @Value("${" + JDBC_URL_KEY + ":" + JDBC_URL_DEFAULT + "}")
    private String jdbcUrl;

    @NotNull
    @Value("${" + JDBC_USERNAME_KEY + ":" + JDBC_USERNAME_DEFAULT + "}")
    private String jdbcUsername;

    @NotNull
    @Value("${" + HIBERNATE_LITEMEMBER_KEY + ":" + HIBERNATE_LITEMEMBER_DEFAULT + "}")
    private String liteMember;

    @NotNull
    @Value("${" + HIBERNATE_MINPUTS_KEY + ":" + HIBERNATE_MINPUTS_DEFAULT + "}")
    private String minimalPuts;

    @NotNull
    @Value("${" + PASSWORD_ITERATION_KEY + ":" + PASSWORD_ITERATION_DEFAULT + "}")
    private Integer passwordIteration;

    @NotNull
    @Value("${" + PASSWORD_SECRET_KEY + ":" + PASSWORD_SECRET_DEFAULT + "}")
    private String passwordSecret;

    @NotNull
    @Value("${" + HIBERNATE_QCACHE_KEY + ":" + HIBERNATE_QCACHE_DEFAULT + "}")
    private String queryCache;

    @NotNull
    @Value("${" + HIBERNATE_REGPREFIX_KEY + ":" + HIBERNATE_REGPREFIX_DEFAULT + "}")
    private String regionPrefix;

    @Value("${" + SCANNER_INTERVAL_KEY + ":" + SCANNER_INTERVAL_DEFAULT + "}")
    private int scannerInterval;

    @NotNull
    @Value("${" + HIBERNATE_SLCACHE_KEY + ":" + HIBERNATE_SLCACHE_DEFAULT + "}")
    private String secondLevelCash;

    @NotNull
    @Value("${" + SERVER_HOST_KEY + ":" + SERVER_HOST_DEFAULT + "}")
    private String serverHost;

    @Value("${" + SERVER_PORT_KEY + ":" + SERVER_PORT_DEFAULT + "}")
    private int serverPort;

    @Value("${" + SESSION_ITERATION_KEY + ":" + SESSION_ITERATION_DEFAULT + "}")
    private int sessionCycle;

    @NotNull
    @Value("${" + SESSION_SECRET_KEY + ":" + SESSION_SECRET_DEFAULT + "}")
    private String sessionSalt;

}

package ru.vmaksimenkov.tm.constant;

import org.jetbrains.annotations.NotNull;

public class Constant {

    @NotNull
    public static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String SCANNER_INTERVAL_DEFAULT = "5";

    @NotNull
    public static final String SCANNER_INTERVAL_KEY = "scanner.interval";

    @NotNull
    public final static String PID_FILENAME = "task-manager.pid";

    @NotNull
    public static final String COMMANDS = "COMMANDS";

    @NotNull
    public static final String COMMANDS_FILE = "./commands.txt";

    @NotNull
    public static final String ERRORS = "ERRORS";

    @NotNull
    public static final String ERRORS_FILE = "./errors.txt";

    @NotNull
    public static final String LOGGER_FILE = "/logger.properties";

    @NotNull
    public static final String MESSAGES = "MESSAGES";

    @NotNull
    public static final String MESSAGES_FILE = "./messages.txt";

    @NotNull
    public static final String EXIT_COMMAND = "exit";

}

package ru.vmaksimenkov.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.service.IPropertyService;

import static ru.vmaksimenkov.tm.constant.Constant.*;

@Getter
@Service
@PropertySource("classpath:" + FILE_NAME)
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("${" + APPLICATION_VERSION_KEY + ":" + APPLICATION_VERSION_DEFAULT + "}")
    private String applicationVersion;

    @NotNull
    @Value("${" + PASSWORD_ITERATION_KEY + ":" + PASSWORD_ITERATION_DEFAULT + "}")
    private Integer passwordIteration;

    @NotNull
    @Value("${" + PASSWORD_SECRET_KEY + ":" + PASSWORD_SECRET_DEFAULT + "}")
    private String passwordSecret;

    @Value("${" + SCANNER_INTERVAL_KEY + ":" + SCANNER_INTERVAL_DEFAULT + "}")
    private int scannerInterval;

}

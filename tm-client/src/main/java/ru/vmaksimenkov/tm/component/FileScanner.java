package ru.vmaksimenkov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.listener.AbstractListener;
import ru.vmaksimenkov.tm.service.PropertyService;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class FileScanner implements Runnable {

    @NotNull
    private final static String FILE_DIRECTORY = "./";
    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
    @NotNull
    @Autowired
    protected PropertyService propertyService;
    @NotNull
    @Autowired
    private AbstractListener[] listeners;
    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    public void init() {
        es.scheduleWithFixedDelay(this, 0, propertyService.getScannerInterval(), TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(FILE_DIRECTORY);
        for (@NotNull final File item : file.listFiles()) {
            if (!item.isFile()) continue;
            @NotNull final String fileName = item.getName();
            boolean check = false;
            for (@NotNull final AbstractListener listener : listeners) {
                if (fileName.equals(listener.command())) check = true;
            }
            if (!check) continue;
            publisher.publishEvent(new ConsoleEvent(fileName));
            item.delete();
        }
    }

    public void stop() {
        es.shutdown();
    }

}

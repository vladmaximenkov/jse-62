package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.TaskRecord;
import ru.vmaksimenkov.tm.event.ConsoleEvent;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.out;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @Override
    @EventListener(condition = "@taskListListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        out.println("[TASK LIST]");
        out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s %n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT");
        @Nullable final List<TaskRecord> list = taskEndpoint.findTaskAll(sessionService.getSession());
        if (list == null) return;
        @NotNull AtomicInteger index = new AtomicInteger(1);
        list.forEach((x) -> out.printf("%-3s | %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s %n",
                index.getAndIncrement(), x.getId(), x.getStatus(), x.getName(), x.getCreated(), x.getDateStart(), x.getDateFinish(), x.getProjectId())
        );
    }

}

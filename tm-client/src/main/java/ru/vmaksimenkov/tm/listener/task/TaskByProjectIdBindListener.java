package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByProjectIdBindListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Bind task by project id";
    }

    @NotNull
    @Override
    public String command() {
        return "task-bind";
    }

    @Override
    @EventListener(condition = "@taskByProjectIdBindListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[BIND TASK TO PROJECT BY ID]");
        if (taskEndpoint.countTask(sessionService.getSession()) < 1)
            throw new TaskNotFoundException();
        System.out.println("ENTER TASK INDEX:");
        @Nullable final String taskId = taskEndpoint.getTaskIdByIndex(sessionService.getSession(), TerminalUtil.nextNumber());
        if (taskId == null) return;
        System.out.println("ENTER PROJECT ID:");
        taskEndpoint.bindTaskById(sessionService.getSession(), TerminalUtil.nextLine(), taskId);
    }

}

package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByIndexFinishListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish task by index";
    }

    @NotNull
    @Override
    public String command() {
        return "task-finish-by-index";
    }

    @Override
    @EventListener(condition = "@taskByIndexFinishListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        taskEndpoint.finishTaskByIndex(sessionService.getSession(), TerminalUtil.nextNumber());
    }

}

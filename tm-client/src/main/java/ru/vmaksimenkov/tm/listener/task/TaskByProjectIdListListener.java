package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.TaskRecord;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public final class TaskByProjectIdListListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list by project id";
    }

    @NotNull
    @Override
    public String command() {
        return "task-list-by-project-id";
    }

    @Override
    @EventListener(condition = "@taskByProjectIdListListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final List<TaskRecord> taskList = taskEndpoint.findTaskByProjectId(sessionService.getSession(), TerminalUtil.nextLine());
        System.out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s |%n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT");
        if (taskList == null) return;
        @NotNull AtomicInteger index = new AtomicInteger(1);
        taskList.forEach(e -> System.out.println(index.getAndIncrement() + ".\t" + e));
    }

}

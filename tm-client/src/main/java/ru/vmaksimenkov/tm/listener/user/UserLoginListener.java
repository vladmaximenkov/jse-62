package ru.vmaksimenkov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.SessionEndpoint;
import ru.vmaksimenkov.tm.endpoint.UserEndpoint;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.exception.entity.UserNotFoundException;
import ru.vmaksimenkov.tm.exception.user.AlreadyLoggedInException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.TerminalUtil.nextLine;

@Component
public final class UserLoginListener extends AbstractUserListener {

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Log in to the system";
    }

    @NotNull
    @Override
    public String command() {
        return "user-login";
    }

    @Override
    @EventListener(condition = "@userLoginListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        if (sessionService.getSession() != null) throw new AlreadyLoggedInException();
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = nextLine();
        if (!userEndpoint.existsUserByLogin(login))
            throw new UserNotFoundException();
        System.out.println("ENTER PASSWORD:");
        sessionService.setSession(
                sessionEndpoint.openSession(login, TerminalUtil.nextLine())
        );
    }

}

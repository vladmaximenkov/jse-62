package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByIndexStartListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start task by index";
    }

    @NotNull
    @Override
    public String command() {
        return "task-start-by-index";
    }

    @Override
    @EventListener(condition = "@taskByIndexStartListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        taskEndpoint.startTaskByIndex(sessionService.getSession(), TerminalUtil.nextNumber());
    }

}

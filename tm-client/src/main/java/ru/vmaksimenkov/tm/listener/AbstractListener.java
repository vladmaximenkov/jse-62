package ru.vmaksimenkov.tm.listener;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.event.ConsoleEvent;

@NoArgsConstructor
public abstract class AbstractListener {

    @Nullable
    public abstract String argument();

    @Nullable
    public abstract String description();

    @NotNull
    public abstract String command();

    public abstract void handler(@NotNull final ConsoleEvent event);

}
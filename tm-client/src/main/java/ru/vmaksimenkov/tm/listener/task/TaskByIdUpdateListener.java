package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Component
public final class TaskByIdUpdateListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id";
    }

    @NotNull
    @Override
    public String command() {
        return "task-update-by-id";
    }

    @Override
    @EventListener(condition = "@taskByIdUpdateListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        if (!taskEndpoint.existsTaskById(sessionService.getSession(), id))
            throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        taskEndpoint.updateTaskById(sessionService.getSession(), id, name, TerminalUtil.nextLine());

    }

}

package ru.vmaksimenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.Status;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectByNameSetStatusListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Set project status by name";
    }

    @NotNull
    @Override
    public String command() {
        return "project-set-status-by-name";
    }

    @Override
    @EventListener(condition = "@projectByNameSetStatusListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        if (!projectEndpoint.existsProjectByName(sessionService.getSession(), name))
            throw new ProjectNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        projectEndpoint.setProjectStatusByName(sessionService.getSession(), name, Status.valueOf(TerminalUtil.nextLine()));
    }

}

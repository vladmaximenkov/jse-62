package ru.vmaksimenkov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.UserEndpoint;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.exception.empty.EmptyEmailException;
import ru.vmaksimenkov.tm.exception.user.EmailExistsException;
import ru.vmaksimenkov.tm.exception.user.LoginExistsException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Component
public final class UserRegistryListener extends AbstractUserListener {

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Register new user";
    }

    @NotNull
    @Override
    public String command() {
        return "user-registry";
    }

    @Override
    @EventListener(condition = "@userRegistryListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        if (userEndpoint.existsUserByLogin(login)) throw new LoginExistsException();
        System.out.println("ENTER E-MAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (userEndpoint.existsUserByEmail(email)) throw new EmailExistsException();
        System.out.println("ENTER PASSWORD:");
        userEndpoint.registryUser(login, TerminalUtil.nextLine(), email);
    }

}

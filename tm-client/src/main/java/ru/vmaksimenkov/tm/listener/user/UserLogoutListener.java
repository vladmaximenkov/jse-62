package ru.vmaksimenkov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.SessionEndpoint;
import ru.vmaksimenkov.tm.endpoint.SessionRecord;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.exception.user.NotLoggedInException;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Log out of the system";
    }

    @NotNull
    @Override
    public String command() {
        return "user-logout";
    }

    @Override
    @EventListener(condition = "@userLogoutListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final SessionRecord session = sessionService.getSession();
        if (session == null) throw new NotLoggedInException();
        sessionEndpoint.closeSession(session);
        sessionService.setSession(null);
    }

}
